## 27 July 2018 - v1.1.5 ##
* Fixing flickering background

## 25 July 2018 - v1.1.4 ##
* Tidy up code and script sitewide

## 24 July 2018 - v1.1.3 ##
* Tidy up code and script in homepage room section

## 24 July 2018 - v1.1.2 ##
* Tidy up code in homepage room section

## 17 July 2018 - v1.1.1 ##
* Tidy up booking form for mobile

## 29 June 2018 - v1.1.0 ##
* Tidy up code
* Tidy up style
* Add alternative custom templates

## 11 April 2018 - v1.0.1 ##
* Fix child theme source

## 1 March 2018 - v1.0.0 ##
* Initial Release
