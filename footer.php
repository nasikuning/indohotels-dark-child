<div class="clearfix"></div>
<!-- The content of your page would go here. -->

	<footer class="footer-distributed">
		<div class="container">
			<div class="footer-info">
				<div class="row">
					<div class="col-md-4">
						<?php if ( ! dynamic_sidebar( 'footer1' ) ) : ?>
						<?php endif; ?>
					</div>
					<div class="col-md-4">
						<?php if ( ! dynamic_sidebar( 'footer2' ) ) : ?>
						<?php endif; ?>
					</div>
					<div class="col-md-4">
						<?php if ( ! dynamic_sidebar( 'footer3' ) ) : ?>
						<?php endif; ?>
					</div>
				</div>
			</div><!-- end .footer-info -->

			<div class="footer-icons">
				<?php krs_sn(); ?>
			</div>

		</div><!-- end .container -->
		<div class="footer-credits">
			<?php echo ot_get_option('krs_footcredits'); ?>
		</div>
	</footer>
</div>
<!-- /wrapper -->

<?php wp_footer(); ?>

<?php if(!empty(ot_get_option('krs_map'))) : ?>
<a class="popup-gmaps" href="<?php echo ot_get_option('krs_map'); ?>">
	<span>
		<i class="fas fa-map-marker-alt"></i>
	</span>
</a>
<?php endif; ?>

</body>

</html>
