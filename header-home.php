<!doctype html>
<html <?php language_attributes(); ?> class="no-js">

<head><?php wp_head(); ?></head>

<body <?php body_class(); ?>>
	<!-- header -->
	<header id="homeSliderCarousel" class="carousel slide header-home" role="banner">
		<!-- nav -->
		<nav id="mainNav" class="navbar navbar-custom navbar-fixed-top">
			<div class="container">
				<div class="navbar-header">
					<div class="navbar-brand">
						<a href="<?php echo get_home_url(); ?>" class="logo-def"><img src="<?php echo ot_get_option('krs_logo'); ?>" alt="" class="img-responsive"></a>
	          <a href="<?php echo get_home_url(); ?>" class="logo-alt"><img src="<?php echo ot_get_option('krs_logo2'); ?>" alt="" class="img-responsive"></a>
						<?php //krs_headlogo(); ?>
					</div>
					<button id="nav-menu-mobile" type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"
					    aria-expanded="false">
						<div id="navbar-hamburger">
							<span class="sr-only">Toggle navigation</span> Menu
							<i class="fa fa-bars"></i>
						</div>
						<div id="navbar-close" class="hidden"><span class="glyphicon glyphicon-remove"></span></div>
					</button>
				</div>
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<?php karisma_nav(); ?>
				</div>
				<div class="wraplang">
					<?php if ( ! dynamic_sidebar( 'box-language' ) ) : ?>
					<?php endif; ?>
				</div>
			</div>
		</nav>
		<!-- /nav -->
		<!-- Wrapper for Slides -->

		<div class="carousel-inner">
			<?php
			if ( function_exists( 'ot_get_option' ) ) {
				$images = explode( ',', ot_get_option( 'krs_gallery', '' ) );
				if ( ! empty( $images ) ) {
					$i=0;
					foreach( $images as $id ) {
						if ( ! empty( $id ) ) {
							if($i++ == 0) {
								$active = 'active';
							} else {$active = '';}
							$full_img_src = wp_get_attachment_image_src( $id, 'gallery-slide-main' );
							$thumb_img_src = wp_get_attachment_image_src( $id, 'gallery-slide-main' );
							?>
							<div class="item <?php echo $active; ?>">
								<div class="home-slider" style="background-image: url('<?php echo $thumb_img_src[0]; ?>');"></div>
							</div>
							<?php
						}
					}
				}
			}
			?>

			<?php /*
			<div class="text-header">
				<h2><?php bloginfo('name'); ?></h2>
				<?php echo ot_get_option('krs_address'); ?>
				<?php if(!empty(ot_get_option('krs_phone'))) : ?>
				<p>Phone:<?php echo ot_get_option('krs_phone'); ?></p>
				<?php endif; ?>
			</div><!-- end .text-header -->
			*/?>

			<a class="left carousel-control" href="#homeSliderCarousel" data-slide="prev"><span class="icon-prev"></span></a>
			<a class="right carousel-control" href="#homeSliderCarousel" data-slide="next"><span class="icon-next"></span></a>

		</div><!-- end .carousel-inner -->

		<ol class="carousel-indicators">
			<?php
			if ( function_exists( 'ot_get_option' ) ) {
				$images = explode( ',', ot_get_option( 'krs_gallery', '' ) );
				if ( ! empty( $images ) ) {
					$i=-1;
					foreach( $images as $id ) {
						if ( ! empty( $id ) ) {
							if($i++ == -1) {
								$active = 'active';
							} else {$active = '';}
							?>
							<li data-target="#homeSliderCarousel" data-slide-to="<?php echo $i; ?>" class="<?php echo $active; ?>"></li>
							<?php
						}
					}
				}
			}
			?>
		</ol>

		<div class="booking-square"></div>
		<div class="booking-slide"></div>
		<section class="booking-section">
	    <div class="booking-box">
        <?php do_shortcode("[booking_engine]"); ?>
	    </div>
	  </section>

	</header>
	<!-- /header -->
