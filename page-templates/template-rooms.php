<?php
/* Template Name: Rooms Template */ get_header('image'); ?>

<main role="main">
	<div class="container text-center"> <!-- container -->
		<!-- section -->
		<section class="inner-page">
				<?php
				$paged=(get_query_var('paged')) ? get_query_var('paged') : 1; 
				$args = array(
					'post_type'=>'rooms',
					'paged' => $paged
				);
				query_posts($args);
				if (have_posts()): while (have_posts()) : the_post(); ?>
				<div class="box-container">
					<div class="room-thumb thumbnail">
						<!-- article -->
						<article id="post-<?php the_ID(); ?>" <?php post_class('rooms-post'); ?>>
							<div class="row">
								<div class="col-md-4">
									<div class="thumb">
										<?php if ( has_post_thumbnail()) :  ?>
											<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
												<?php the_post_thumbnail(array(300,150)); ?>
											</a>
										<?php endif; ?>
									</div>
								</div>
								<div class="col-md-8">
									<div class="box-text">
										<h2 class="title-room-list"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
										<ul class="room-info">
											<li>
												<span class="room-value">Room Size</span><span> : </span>
												<span><?php echo rwmb_meta( 'room_size' ); ?> m<sup>2</sup></span>
											</li>
											<li>
												<span><span class="room-value">View</span><span> : </span>
												<span><?php echo rwmb_meta( 'room_view' ); ?></span>
											</li>
											<li>
												<span><span class="room-value">Ocupancy </span><span> : </span>
												<span><?php echo rwmb_meta( 'room_occupancy' ); ?> Person</span>
											</li>
											<li>
												<span><span class="room-value">Bed Size</span><span> : </span>
												<span><?php echo rwmb_meta( 'bed_size' ); ?></span>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</article>
					</div>
				</div>
				<?php endwhile; ?>
				<?php else: ?>
					<article>
						<h2><?php _e( 'Sorry, nothing to display.', karisma_text_domain ); ?></h2>
					</article>
				<?php endif; ?>
				<?php karisma_pagination(); ?>
				

		</section>
		<!-- /section -->
</div> <!-- end container -->
</main>

<?php get_footer(); ?>
