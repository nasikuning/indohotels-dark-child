<?php
/* Template Name: Review Template */ get_header('image'); ?>

<main role="main">
	<div class="container text-center">

		<section class="inner-page owl-carousel home-text-slide">

			<?php
				$args = array(
					'post_type' => 'hotel-info',
					'category_name' => 'testimonial',
				);
				query_posts($args);
				if (have_posts()): while (have_posts()) : the_post();
			?>

			<div class="box-container">
				<div class="box-review">
					<article id="post-<?php the_ID(); ?>" <?php post_class( 'rooms-post'); ?>>
						<div class="thumb">
							<?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
								<?php the_post_thumbnail('gallery-slide'); // Declare pixel size you need inside the array ?>
							<?php endif; ?>
						</div>
						<div class="box-text">
							<h4><?php the_title(); ?></h4>
							<?php the_content(); ?>
						</div><!-- end .box-text -->
					</article>
				</div><!-- end .box-review -->
			</div><!-- end .box-container -->

			<?php endwhile; ?>
			<?php else: ?>

			<article>
				<h2><?php _e( 'Sorry, nothing to display.', karisma_text_domain ); ?></h2>
			</article>

			<?php endif; ?>

			<?php get_template_part('pagination'); ?>

		</section><!-- end .section -->
	</div><!-- end container -->
</main>

<?php get_footer(); ?>
