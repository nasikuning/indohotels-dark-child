<?php /* Template Name: Basic Template */ get_header('image'); ?>

	<main role="main" class="col-md-12">
		<div class="container">
			<section class="inner-page">
			<?php if (have_posts()): while (have_posts()) : the_post(); ?>

				<!-- article -->
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<?php the_content(); ?>
				</article>
				<!-- /article -->

				<?php endwhile; ?>
				<?php else: ?>

				<!-- article -->
				<article>
					<h3 class="title text-center"><?php _e( 'Sorry, nothing to display.', 'indohotels' ); ?></h3>
				</article>
				<!-- /article -->

			<?php endif; ?>

			</section>
			<!-- /section -->
		</div><!-- end .container -->
	</main>


<?php get_footer(); ?>
