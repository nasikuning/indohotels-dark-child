<?php /* Template Name: Maps Template */ get_header('image'); ?>


<main role="main" class="col-md-12">
	<section class="container">


		<?php if (have_posts()): while (have_posts()) : the_post(); ?>
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<div class="row">
					<div class="col-md-6 box">
							<?php the_content(); ?>
					</div>
					<div class="col-md-6">
						<?php echo rwmb_meta('map'); ?>
					</div>
				</div>
				<?php //echo rwmb_meta('map'); ?>
				<?php //the_content(); ?>

				<br class="clear">
                <?php if(!empty(rwmb_meta('contact_title'))) : ?>
				<div class="contact-box col-md-12">
					<div class="contact-headline">
						<h3><?php echo rwmb_meta('contact_title'); ?></h3>
					</div>
					<div class="contact-address">
						<?php echo rwmb_meta('contact_address'); ?>
					</div>
					<div class="contact-info">
						<table>
							<tbody>
                            <?php if(!empty(rwmb_meta( 'contact_phone' ))): ?>
								<tr>
									<td><i class="fa fa-phone" aria-hidden="true"></i></td>
									<td>Phone</td><td>:</td>
									<td>
										<ul>								
											<?php 
											$values = rwmb_meta( 'contact_phone' );
											foreach ( $values as $value )
											{
												echo '<li>'. $value . '</li>';
											}
											?>
										</ul>
									</td>
								</tr>
                                <?php endif; ?>
                                <?php if(rwmb_meta( 'contact_mobile' )) : ?>
								<tr>
									<td><i class="fa fa-mobile" aria-hidden="true"></i></td>
									<td>Mobile</td><td>:</td>
									<td>
										<ul>									
											<?php 
											$values = rwmb_meta( 'contact_mobile' );
											foreach ( $values as $value )
											{
												echo '<li>'. $value . '</li>';
											}
											?>
										</ul>
									</td>
								</tr>
                                <?php endif; ?>
                                <?php if(!empty(rwmb_meta( 'contact_fax' ))): ?>
								<tr>
									<td><i class="fa fa-fax" aria-hidden="true"></i></td>
									<td>Fax</td><td>:</td>
									<td>
										<ul>								
											<?php 
											$values = rwmb_meta( 'contact_fax' );
											foreach ( $values as $value )
											{
												echo '<li>'. $value . '</li>';
											}
											?>
										</ul>							
									</td>	
								</tr>
                                <?php endif; ?>
                                <?php if(!empty(rwmb_meta( 'contact_email' ))) : ?>
								<tr>
									<td><i class="fa fa-envelope" aria-hidden="true"></i></td>
									<td>Email</td><td>:</td>
									<td>
										<ul>			
											<?php 
											$values = rwmb_meta( 'contact_email' );
											foreach ( $values as $value )
											{
												echo '<li>'. $value . '</li>';
											}
											?>
										</ul>	
									</td>
								</tr>
                                <?php endif; ?>
							</tbody>
						</table>
					</div>
				</div>
                <?php endif; ?>
			</article>

		<?php endwhile; ?>

	<?php else: ?>

		<article>

			<h2 class="title text-center"><?php _e( 'Sorry, nothing to display.', karisma_text_domain ); ?></h2>

		</article>

	<?php endif; ?>

</section>
</main>

<?php get_footer(); ?>
