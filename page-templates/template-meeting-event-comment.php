<?php
/* Template Name: Meeting Event with Comment Template */ get_header('image'); ?>

<main role="main" class="col-md-12">
	<div class="container text-center">
		<!-- container -->
		<!-- section -->
		<section>
			<?php

			// query_posts($args);
			if (have_posts()): while (have_posts()) : the_post(); ?>

			<div class="box-container">
				<div class="room-thumb ">
					<article id="post-<?php the_ID(); ?>" <?php post_class( 'rooms-post'); ?>>
						<?php the_content(); ?>
					</article>
				</div>
			</div>
			
			<?php // If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;
			?>

			<?php endwhile; ?>

			<?php else: ?>

			<!-- article -->
			<article>
				<h2>
					<?php _e( 'Sorry, nothing to display.', karisma_text_domain ); ?>
				</h2>
			</article>
			<!-- /article -->

			<?php endif; ?>
			<?php wp_reset_query();?>
			<?php get_template_part('pagination'); ?>

		</section>
		<!-- /section -->
	</div>
	<!-- end container -->
</main>

<?php get_footer(); ?>
