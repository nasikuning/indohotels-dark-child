<?php /* Template Name: Gallery Template */ get_header('image'); ?>

<main role="main">

	<div class="container">
		<section class="inner-page">
			<?php
				$args = array('post_type'=>'gallery','posts_per_page'=> -1);
				query_posts($args);
			?>
			<div class="box-gallery">
				<div class="gallery-view">
					<!-- Slider -->
					<?php if (have_posts()):
						$i=0;
						while (have_posts()) : the_post();
					?>
					<div class="item <?php echo ($i==0)?'active':'' ?>" data-slide-number="<?php echo $i; ?>">
						<div class="slider-item">
							<?php the_post_thumbnail('gallery-slide');?>
						</div>
					</div>
					<?php $i++; ?>
					<?php endwhile; ?>
					<?php endif; ?>
				</div><!-- end .gallery-view -->

				<div class="gallery-nav">
					<?php if (have_posts()):
						$i=0;
						while (have_posts()) : the_post();
					?>
					<div class="item <?php echo ($i==0)?'active':'' ?>" data-slide-number="<?php echo $i; ?>">
						<?php the_post_thumbnail('gallery-slide');?>
					</div>
					<?php $i++; ?>
					<?php endwhile; ?>
					<?php endif; ?>
				</div> <!-- end .gallery-nav -->
			</div><!-- end .box-gallery -->
		</section><!-- /section -->
	</div><!-- end .container -->

</main>

<?php //get_sidebar(); ?>

<?php get_footer(); ?>
