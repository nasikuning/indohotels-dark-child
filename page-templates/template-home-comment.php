<?php /* Template Name: Home Comment Template */ get_header('home'); ?>
<main role="main">

  <section id="welcome-hotel">
    <div class="container">
      <div class="box-bg">
        <h2><?php echo ot_get_option('krs_ros_in_title');?></h2>
        <span class="line"></span>
        <?php echo ot_get_option('krs_ros_in'); ?>
      </div>
    </div>
  </section>

  <?php top_deals('Promotions'); ?>

    <!-- section -->
    <?php if (ot_get_option('krs_room_actived') != 'off') : ?>
    <section id="home-featured" style="background-image: linear-gradient(rgba(0, 0, 0, 0), rgba(0, 0, 0, 0)), url(<?php echo ot_get_option('krs_background_text1'); ?>);">
      <div class="container">

        <?php
         $args = array(
          'post_type'=> ot_get_option('krs_section_2'),
          'posts_per_page' => 9,
        );

        $krs_query = new WP_Query($args);

        $count = $krs_query->post_count;

        if (($count == 5)) {
            $coli = 'ods';
        } else {
            $coli = '';
        }

        if (($count == 2) || ($count == 4)) {
            $col = 'col-md-6';
        } else {
            $col = 'col-md-4';
        }

        if ($krs_query->have_posts()): ?>
        <div class="box-bg <?php echo $coli; ?>" <?php if ($col == 'col-md-4') {
            echo 'style="width: 70%";';
        } ?>>
          <h3><?php echo ot_get_option('krs_headline_section2');?></h3>
          <span class="line"></span>
          <div class="row">
            <?php while ($krs_query->have_posts()) : $krs_query->the_post(); ?>
            <div class="<?php echo $col ?>">
              <div class="box-room">
                <div class="thumb">
                  <?php if (has_post_thumbnail()) : // Check if thumbnail exists?>
                  <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                      <?php the_post_thumbnail('medium'); // Declare pixel size you need inside the array?>
                  </a>
                  <?php endif; ?>
                </div>
                <h4><?php the_title(); ?></h4>
              </div>
            </div>
            <?php endwhile; ?>
          </div>
            <?php endif; ?>

        </div> <!-- /box-bg -->
      </div>
    </section>
    <?php endif; ?>

    <?php if (ot_get_option('krs_section3_actived') != 'off') : ?>
    <section class="hotel-property text-center">
      <div class="container">
        <?php
              $args = array(
                  'post_type' => 'property',
                );
              query_posts($args);
              if (have_posts()) : ?>
          <h3><?php echo ot_get_option('krs_headline_section3');?></h3>
          <span class="line"></span>
          <div class="row">
            <?php while (have_posts()) : the_post(); ?>
            <div class="item col-md-4">
              <div class="thumbnails">
                <?php if (has_post_thumbnail()) : ?>
                <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                    <img class="img-responsive" src="<?php the_post_thumbnail_url('gallery-slide'); ?>" />
                </a>
                <?php endif; ?>
              </div>
              <h4><?php echo get_the_title(); ?></h4>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
            <div class="clearfix"></div>
          </div>
      </div>
    </section>
    <?php endif; ?>

    <!-- gallery -->
    <section id="image-popups" class="home-galleries text-center">
      <?php
        $args = array(
            'post_type' => 'gallery',
            'phototype'  => 'home',
            );
        query_posts($args);
        if (have_posts()) : ?>
      <h3><?php _e('Image Gallery', karisma_text_domain); ?></h3>
      <span class="line"></span>
      <div class="box-home-grid">
        <?php while (have_posts()) : the_post(); ?>
        <div class="item col-md-3 col-sm-3">
          <div class="thumbnails bximage">
            <?php if (has_post_thumbnail()) : ?>
            <a href="<?php the_post_thumbnail_url('gallery-slide'); ?>" title="<?php the_title_attribute(); ?>">
                <img class="image-popups" src="<?php the_post_thumbnail_url('gallery-slide'); ?>" />
                <div class="overlay"><span><?php the_title_attribute(); ?></span></div>
            </a>
            <?php endif; ?>
          </div>
        </div>
        <?php endwhile; ?>
        <?php endif; ?>
      <div class="clearfix"></div>
    </section>
    <!-- end gallery -->

    <?php footer_slide(); ?>

    <section class="home-score text-center">
      <div class="container">
      <?php
            $args = array(
          'post_type' => 'hotel-info',
          'category_name'  => 'score',
                );
            query_posts($args);
            if (have_posts()) :
      ?>
      <div class="box-home-grid">
        <?php while (have_posts()) : the_post(); ?>
        <div class="item col-md-3 col-sm-3">
          <div class="thumbnails">
            <?php if (has_post_thumbnail()) : ?>
              <img src="<?php the_post_thumbnail_url('gallery-slide'); ?>" />
            <?php endif; ?>
            <?php the_content(); ?>
          </div>
        </div>
        <?php endwhile; ?>
        <?php endif; ?>
        <?php wp_reset_query();?>
      </div>
    </section>

  <?php if (have_posts()): while (have_posts()) : the_post(); ?>
  <section class="box-comment">
    <div class="container">
      <?php // If comments are open or we have at least one comment, load up the comment template.
  			if ( comments_open() || get_comments_number() ) :
  				comments_template();
  			endif;
  		?>
    </div>
  </section>
  <?php endwhile; ?>
  <?php endif; ?>

</main>
<?php get_footer(); ?>
