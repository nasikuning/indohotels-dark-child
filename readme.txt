=== Indohotels Dark Child ===
Contributors: Amri Karisma
Donate link: https://www.amrikarisma.com/
Tags: simple, seo
Requires at least: 4.8
Stable tag: v1.1.5
Version: 1.1.5
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html


== Description ==
Indohotels Dark Child is a child theme of Indohotelswp


== Installation ==

This section describes how to install the child theme and get it working.

1. You need to install and activate the parent theme first, indohotelswp
2. In your admin panel, go to Appearance -> Themes and click the 'Add New' button.
3. Click on the 'Activate' button to use your new theme right away.
4. Navigate to Appearance > Customize in your admin panel and customize to taste.


== Changelog ==

= 1.1.5 =
* fixing flickering background

= 1.1.4 =
* Tidy up code and script sitewide

= 1.1.3 =
* Tidy up code and script in homepage room section

= 1.1.2 =
* Tidy up code in homepage room section

= 1.1.1 =
* Tidy up booking engine mobile

= 1.1.0 =
* Tidy up code
* Tidy up style
* Add alternative custom templates

= 1.0.1 =
* Fix child theme source

= 1.0.0 =
* Initial Release
