<?php get_header('image'); ?>

<main role="main">
	<!-- section -->
	<section class="container">

		<?php if (have_posts()): while (have_posts()) : the_post(); ?>

		<!-- article -->
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<div class="box-book-rooms">
				<!-- box booking details -->
				<div class="room-details">

						<div class="room-booking">
							<div class="room-box">
								<!-- Slider -->
								<div id="slider">
									<!-- Top part of the slider -->
									<div id="carousel-bounding-box">
										<div class="carousel slide" id="myCarousel">
											<!-- Carousel items -->
											<div class="carousel-inner">
												<?php
													$images = rwmb_meta( 'indohotels_imgadv', 'size=gallery-slide-main' );
													if ( !empty( $images ) ) {
														$i = 0;
														foreach ( $images as $image ) {
															if($i++ == 0) {
																$active = 'active';
															} else {$active = '';}
															echo '<div class="'. $active .' item">';
															echo '<img src="', esc_url( $image['url'] ), '"  alt="', esc_attr( $image['alt'] ), '">';
															echo '</div>';
														}
													}
													?>
											</div>
											<!-- Carousel nav -->
											<a class="carousel-control left" data-slide="prev" href="#myCarousel"><span>‹</span></a>
											<a class="carousel-control right" data-slide="next" href="#myCarousel"><span>›</span></a>
										</div>
									</div>
								</div>
								<!--/Slider-->
							</div>

							<div class="room-details-desc">
								<div class="row">
									<div class="col-md-12">
										<ul class="room-info">
											<?php if(!empty(rwmb_meta( 'room_size' ))) : ?>
											<li>
												<span class="room-value">Room Size</span><span> : </span>
												<span><?php echo rwmb_meta( 'room_size' ); ?> m<sup>2</sup></span>
											</li>
											<?php endif; ?>
											<?php if(!empty(rwmb_meta( 'balcony_size' ))) : ?>
											<li>
												<span class="room-value">Balcony Size</span><span> : </span>
												<span><?php echo rwmb_meta( 'balcony_size' ); ?> m<sup>2</sup></span>
											</li>
											<?php endif; ?>
											<?php if(!empty(rwmb_meta( 'room_view' ))) : ?>
											<li>
												<span><span class="room-value">View</span><span> : </span>
												<span><?php echo rwmb_meta( 'room_view' ); ?></span>
											</li>
											<?php endif; ?>
											<?php if(!empty(rwmb_meta( 'room_occupancy' ))) : ?>
											<li>
												<span><span class="room-value">Pax </span><span> : </span>
												<span><?php echo rwmb_meta( 'room_occupancy' ); ?> Person</span>
											</li>
											<?php endif; ?>
											<?php if(!empty(rwmb_meta( 'bed_size' ))) : ?>
											<li>
												<span><span class="room-value">Bed Size</span><span> : </span>
												<span><?php echo rwmb_meta( 'bed_size' ); ?></span>
											</li>
											<?php endif; ?>
										</ul>
									</div>
									<div class="col-md-12">
										<?php the_content(); ?>
									</div>
								</div>
							</div><!-- end .room-detail -->

							<?php if(rwmb_meta( 'room_convenience' ) || rwmb_meta( 'room_indulgence' ) || rwmb_meta( 'room_comfort' )): ?>
							<h3 class="room-details-f-title"><?php _e('Room features', karisma_text_domain); ?></h3>
							<div class="feature-box col-md-12">

								<?php if(!empty(rwmb_meta( 'room_convenience' ))): ?>
								<div class="col-md-4">
									<h4>
										<?php _e('For Your Convenience', karisma_text_domain); ?>
									</h4>
									<ul class="room-features">
										<?php
											$values = rwmb_meta( 'room_convenience' );
											foreach ( $values as $value )
											{
												echo '<li>'. $value . '</li>';
											}
											?>

									</ul>
								</div>
								<?php endif; ?>
								<?php if(!empty(rwmb_meta( 'room_indulgence' ))): ?>
								<div class="col-md-4">
									<h4>
										<?php _e('For Your Indulgence', karisma_text_domain); ?>
									</h4>

									<ul class="room-features">
										<?php
											$values = rwmb_meta( 'room_indulgence' );
											foreach ( $values as $value )
											{
												echo '<li>'. $value . '</li>';
											}
											?>
									</ul>
								</div>
								<?php endif; ?>
								<?php if(!empty(rwmb_meta( 'room_comfort' ))): ?>
								<div class="col-md-4">
									<h4>
										<?php _e('For Your Comfort', karisma_text_domain); ?>
									</h4>

									<ul class="room-features">
										<?php
											$values = rwmb_meta( 'room_comfort' );
											foreach ( $values as $value )
											{
												echo '<li>'. $value . '</li>';
											}
											?>
									</ul>
								</div>
								<?php endif; ?>

							</div><!-- end .feature-box -->
							<?php endif; ?>
							<?php
							$data['propery_id'] = get_option('idn_booking_engine.propery_id');
							?>
							<div class="text-center">
								<a href="//www.indohotels.id/website/property/<?php echo $data['propery_id']; ?>" class="btn btn-check"><?php _e('Check Availability', karisma_text_domain); ?>
								</a>
							</div>

						</div><!-- end .room-booking -->

				</div>
			</div>
		</article>
		<!-- /article -->

		<?php // If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;
		?>

		<?php endwhile; ?>

		<?php else: ?>

		<!-- article -->
		<article>

			<h1>
				<?php _e( 'Sorry, nothing to display.', karisma_text_domain ); ?>
			</h1>

		</article>
		<!-- /article -->





		<?php endif; ?>

	</section>
	<!-- /section -->
</main>

<?php get_footer(); ?>
